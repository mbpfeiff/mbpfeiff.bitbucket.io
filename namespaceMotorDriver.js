var namespaceMotorDriver =
[
    [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ],
    [ "ch1", "namespaceMotorDriver.html#acfe37823bfa3208e2660800b88bfa3f3", null ],
    [ "ch2", "namespaceMotorDriver.html#ac7a88c3c43f3cf29ccb32b8338e7dfe1", null ],
    [ "ch3", "namespaceMotorDriver.html#ac31ce1284196d9fb25dac24d35a7a05f", null ],
    [ "ch4", "namespaceMotorDriver.html#aa8cbd5992127c00a7266adc29d69dabe", null ],
    [ "mode", "namespaceMotorDriver.html#a2cf2d528509349d585dcb5235f13cb8a", null ],
    [ "moe1", "namespaceMotorDriver.html#a9db326c60d70c465970abb796624d53b", null ],
    [ "moe2", "namespaceMotorDriver.html#ac2231cbd9bf4f7dd64e48a69dc620d52", null ],
    [ "nfaultset", "namespaceMotorDriver.html#afedbb7852e89a521ce26a6d32f0756e7", null ],
    [ "OUT_PP", "namespaceMotorDriver.html#a12f932b793f3fe5aa8accff7a237d4da", null ],
    [ "pin_IN1", "namespaceMotorDriver.html#ae7fab324157659601bb6e37dff60c317", null ],
    [ "pin_IN2", "namespaceMotorDriver.html#a2c6d581f7aec19082cd5a0deaf06c8af", null ],
    [ "pin_IN3", "namespaceMotorDriver.html#a47a20954cff9e8ec7b1c8d2018a7431c", null ],
    [ "pin_IN4", "namespaceMotorDriver.html#abeb3a37b6274861ba3d9b33303cc19fc", null ],
    [ "pin_nFAULT", "namespaceMotorDriver.html#a606ec75edbf27706f727a2ac51c9923e", null ],
    [ "pin_nSLEEP", "namespaceMotorDriver.html#abcf033e57bd2c6b8a7ba2a7f4f36afc3", null ],
    [ "timer", "namespaceMotorDriver.html#a995faa5201ef138d0bde532d2c8031be", null ],
    [ "value", "namespaceMotorDriver.html#a2b795811d3e12148c1b562b2f9579f82", null ]
];