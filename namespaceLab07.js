var namespaceLab07 =
[
    [ "TouchControl", "classLab07_1_1TouchControl.html", "classLab07_1_1TouchControl" ],
    [ "A0", "namespaceLab07.html#ac1ed62d117157ff61e2aca481f393bc1", null ],
    [ "A1", "namespaceLab07.html#ae4ccb875307bb329bc932e98c0f77b4e", null ],
    [ "A6", "namespaceLab07.html#a5cb594c29e4a5f0d6e4029644913eae6", null ],
    [ "A7", "namespaceLab07.html#a209580649273dc17c5a9850f6836c9ef", null ],
    [ "avetime", "namespaceLab07.html#a41e1d8f7945a0c738d458a90e76d29f2", null ],
    [ "CoordCent", "namespaceLab07.html#a29094e93b5428e48cf22dd79c198492e", null ],
    [ "endtime", "namespaceLab07.html#a2deecf898550da14278dbdcaba732fd0", null ],
    [ "Length", "namespaceLab07.html#a8a13b865417dc55488ef842d5a9601d6", null ],
    [ "saveval", "namespaceLab07.html#a299210b4bad914a5665bf43cdd7aa249", null ],
    [ "starttime", "namespaceLab07.html#a5d101ea18029293dc48dc4076f2a52f3", null ],
    [ "timestore", "namespaceLab07.html#a89157272480ddc482401b86afd57cbee", null ],
    [ "TouchScreen", "namespaceLab07.html#ade25c7cf4ffb3c545cc11c58b185db14", null ],
    [ "Width", "namespaceLab07.html#a110349236a10eaf5c49088ea7a2c7220", null ]
];