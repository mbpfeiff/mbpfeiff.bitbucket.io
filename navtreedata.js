/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Matthew Pfeiffer's ME 405 Project Portfolio", "index.html", [
    [ "Portfolio Details", "index.html#sec_port", null ],
    [ "Lab0x01 Documentation", "index.html#sec_lab1", [
      [ "Introduction", "index.html#sec_intro1", null ]
    ] ],
    [ "Lab0x02 Documentation", "index.html#sec_lab2", [
      [ "Introduction", "index.html#sec_intro2", null ]
    ] ],
    [ "Lab0x03 Documentation", "index.html#sec_lab3", [
      [ "Introduction", "index.html#sec_intro3", null ]
    ] ],
    [ "Lab0x04 Documentation", "index.html#sec_lab4", [
      [ "Introduction", "index.html#sec_intro4", null ],
      [ "Comments", "index.html#sec_notes", null ],
      [ "Physical-Connection", "index.html#sec_phycon", null ],
      [ "Temperature vs.Time Plot", "index.html#sec_plots", null ]
    ] ],
    [ "Lab0x05 Documentation", "index.html#sec_lab5", [
      [ "Introduction", "index.html#sec_intro5", null ]
    ] ],
    [ "Lab0x07 Documentation", "index.html#sec_lab7", null ],
    [ "Lab0x08 Documentation", "index.html#sec_lab8", [
      [ "Introduction", "index.html#sec_intro", null ],
      [ "Comments", "index.html#Comments", null ],
      [ "References", "index.html#Ref", null ]
    ] ],
    [ "Term Project Documentation", "index.html#sec_lab9", [
      [ "Balancing Platform", "index.html#sec_bal", null ],
      [ "Results", "index.html#Results", null ]
    ] ],
    [ "Analysis", "page1.html", null ],
    [ "Simulation", "page2.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"FindPos_8py.html",
"index.html#Results",
"namespacemembers_func.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';