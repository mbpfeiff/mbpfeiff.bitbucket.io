var annotated_dup =
[
    [ "cotask", "namespacecotask.html", [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "encoder", "namespaceencoder.html", [
      [ "EncoderDriver", "classencoder_1_1EncoderDriver.html", "classencoder_1_1EncoderDriver" ]
    ] ],
    [ "FindPos", "namespaceFindPos.html", [
      [ "TouchControl", "classFindPos_1_1TouchControl.html", "classFindPos_1_1TouchControl" ]
    ] ],
    [ "Lab07", "namespaceLab07.html", [
      [ "TouchControl", "classLab07_1_1TouchControl.html", "classLab07_1_1TouchControl" ]
    ] ],
    [ "MotorDriver", "namespaceMotorDriver.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "MotorDriver-M", "namespaceMotorDriver-M.html", [
      [ "MotorDriver", "classMotorDriver-M_1_1MotorDriver.html", "classMotorDriver-M_1_1MotorDriver" ]
    ] ],
    [ "task_share", "namespacetask__share.html", [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ]
];