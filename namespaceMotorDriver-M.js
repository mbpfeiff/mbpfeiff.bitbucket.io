var namespaceMotorDriver_M =
[
    [ "MotorDriver", "classMotorDriver-M_1_1MotorDriver.html", "classMotorDriver-M_1_1MotorDriver" ],
    [ "ch1", "namespaceMotorDriver-M.html#aeba138f82665e3e1a6ff501a259e1e83", null ],
    [ "ch2", "namespaceMotorDriver-M.html#ad4dd44f129e3aea5ad7739a1a7747644", null ],
    [ "ch3", "namespaceMotorDriver-M.html#a16f9a71bae1a14415530b18b1cf10be3", null ],
    [ "ch4", "namespaceMotorDriver-M.html#a4456e69f5237612eec9e028399f86b52", null ],
    [ "mode", "namespaceMotorDriver-M.html#ae1c745c16f997cd4a5911b5b293c918d", null ],
    [ "moe1", "namespaceMotorDriver-M.html#abb102f9249105e6a5af46352bd1c1144", null ],
    [ "moe2", "namespaceMotorDriver-M.html#a6a54d428c16c769611492439ab8271d7", null ],
    [ "nfaultset", "namespaceMotorDriver-M.html#a8cc2cdcfba6325134c49d6502f7949f4", null ],
    [ "OUT_PP", "namespaceMotorDriver-M.html#acdd8020e38eb25e914c1e75ae6178af6", null ],
    [ "pin_IN1", "namespaceMotorDriver-M.html#a5bd438e73da14eb7f5ec1473e2fe42a1", null ],
    [ "pin_IN2", "namespaceMotorDriver-M.html#a6bb7ef621160b98b72f446a97d60c847", null ],
    [ "pin_IN3", "namespaceMotorDriver-M.html#a9a4ffa142772c619cd67fbd226505633", null ],
    [ "pin_IN4", "namespaceMotorDriver-M.html#af77e83eac87cb82f6ed12b6bc892e78e", null ],
    [ "pin_nFAULT", "namespaceMotorDriver-M.html#a50948a151e794b7214fe4bff20841917", null ],
    [ "pin_nSLEEP", "namespaceMotorDriver-M.html#a6224bb199d232e42d97291c974e00fe4", null ],
    [ "timer", "namespaceMotorDriver-M.html#af3ba5319b018fb5d9445962aee486932", null ],
    [ "value", "namespaceMotorDriver-M.html#a2595eb5005cfce5990c27b5df5643a0f", null ]
];