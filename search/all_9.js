var searchData=
[
  ['i2c_69',['i2c',['../namespacemain4.html#a01bed9afe7377f6f7bd85d37722786c3',1,'main4.i2c()'],['../namespacemcp9808.html#ae99e487519ec535e9356de31b7f1dee7',1,'mcp9808.i2c()']]],
  ['in1_5fpin_70',['IN1_pin',['../classMotorDriver-M_1_1MotorDriver.html#ad54fb4c2764df66d21385e2307e3acb3',1,'MotorDriver-M.MotorDriver.IN1_pin()'],['../classMotorDriver_1_1MotorDriver.html#a7fe6850168920ddc25f38c95899fe945',1,'MotorDriver.MotorDriver.IN1_pin()']]],
  ['in2_5fpin_71',['IN2_pin',['../classMotorDriver-M_1_1MotorDriver.html#af758ca223a6338f7b3bb69903912b482',1,'MotorDriver-M.MotorDriver.IN2_pin()'],['../classMotorDriver_1_1MotorDriver.html#a97bda61202526af5f34138adc5baf7f9',1,'MotorDriver.MotorDriver.IN2_pin()']]],
  ['init_72',['init',['../namespacemcp9808.html#a40358007b075593ab3ff680e18d47e85',1,'mcp9808']]],
  ['init_5fcounter_73',['init_counter',['../classencoder_1_1EncoderDriver.html#a81da512be4a1c6f3aaa7f7f55ad2bb14',1,'encoder::EncoderDriver']]],
  ['initmessage_74',['InitMessage',['../namespaceLab03.html#a6444f9d665a2dc1c3d110aca92cb95da',1,'Lab03.InitMessage()'],['../namespaceUI.html#a09e4ee3058d53a45b3807f666a02a86b',1,'UI.InitMessage()']]],
  ['insufficient_75',['insufficient',['../namespaceLab01.html#abb299376411f5fe0b85f95775d69aa3a',1,'Lab01']]],
  ['interval_76',['interval',['../namespaceencoder.html#ab53c80b1a2a5d9f0711848d784e89227',1,'encoder']]],
  ['isr_5fgain_77',['isr_gain',['../namespaceLab02.html#a109344ccbd9c78c75efa1247fd6e9386',1,'Lab02']]],
  ['itsit_78',['itsit',['../namespacemain3.html#a164c575ee7964f2eec4c34aa59b63114',1,'main3']]]
];
