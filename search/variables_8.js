var searchData=
[
  ['i2c_352',['i2c',['../namespacemain4.html#a01bed9afe7377f6f7bd85d37722786c3',1,'main4.i2c()'],['../namespacemcp9808.html#ae99e487519ec535e9356de31b7f1dee7',1,'mcp9808.i2c()']]],
  ['in1_5fpin_353',['IN1_pin',['../classMotorDriver-M_1_1MotorDriver.html#ad54fb4c2764df66d21385e2307e3acb3',1,'MotorDriver-M.MotorDriver.IN1_pin()'],['../classMotorDriver_1_1MotorDriver.html#a7fe6850168920ddc25f38c95899fe945',1,'MotorDriver.MotorDriver.IN1_pin()']]],
  ['in2_5fpin_354',['IN2_pin',['../classMotorDriver-M_1_1MotorDriver.html#af758ca223a6338f7b3bb69903912b482',1,'MotorDriver-M.MotorDriver.IN2_pin()'],['../classMotorDriver_1_1MotorDriver.html#a97bda61202526af5f34138adc5baf7f9',1,'MotorDriver.MotorDriver.IN2_pin()']]],
  ['init_5fcounter_355',['init_counter',['../classencoder_1_1EncoderDriver.html#a81da512be4a1c6f3aaa7f7f55ad2bb14',1,'encoder::EncoderDriver']]],
  ['interval_356',['interval',['../namespaceencoder.html#ab53c80b1a2a5d9f0711848d784e89227',1,'encoder']]],
  ['isr_5fgain_357',['isr_gain',['../namespaceLab02.html#a109344ccbd9c78c75efa1247fd6e9386',1,'Lab02']]],
  ['itsit_358',['itsit',['../namespacemain3.html#a164c575ee7964f2eec4c34aa59b63114',1,'main3']]]
];
