var Lab02_8py =
[
    [ "ButtonPress", "Lab02_8py.html#a37a79f664a874e74f1907e89130fce03", null ],
    [ "LEDCallback", "Lab02_8py.html#a9566924a602a6b4e160b20404ebf1d23", null ],
    [ "MeasureCallback", "Lab02_8py.html#ab054d484914bf5e10f60c5d31881f19c", null ],
    [ "AVETIME", "Lab02_8py.html#a01c7fd0eab5956f399b30cacd1f975e9", null ],
    [ "ButtonHit", "Lab02_8py.html#adcd7656b3ecf1f7cbcd4fc1c3289981c", null ],
    [ "ButtonInterrupt", "Lab02_8py.html#a74a8d2cb48617bd75113a527c64e8ff3", null ],
    [ "HitAgain", "Lab02_8py.html#a513cfbb7ca8cca921ed86670751fd883", null ],
    [ "isr_gain", "Lab02_8py.html#a109344ccbd9c78c75efa1247fd6e9386", null ],
    [ "LEDTimer", "Lab02_8py.html#a2d3a45a8286b134452bf65aaa6dc13fe", null ],
    [ "Measure", "Lab02_8py.html#a81e809cdf579fc2e318e277553fd6974", null ],
    [ "myButton", "Lab02_8py.html#a7c695dbe8e4b802188ed5c6e083def22", null ],
    [ "myPin", "Lab02_8py.html#ad42cd453b11111424920f6ef628828c5", null ],
    [ "myTim", "Lab02_8py.html#aa4330192b557784b114e3214df3a52ce", null ],
    [ "state", "Lab02_8py.html#a3fbe08d0559d744663d9d9a49ae0d5d6", null ]
];